import os
import stripe
from chalice import Chalice

app = Chalice(app_name='stripe-prototype')

payment_plan_id = os.environ.get('PAYMENT_PLAN_ID', 'plan_CcwDsseYD6gY7n')
stripe.api_key = os.environ.get('STRIPE_API_KEY', 'sk_test_8MWyRiyTsBirGJcvaHUYSWf0')


@app.route('/', methods=['GET'], cors=True)
def index():
    query_params = app.current_request.query_params

    if 'charge_token' in query_params:
        print('checking for charge_token', query_params)
        charge_token = query_params['charge_token']
        print('found', charge_token)
    else:
        print('creating token')
        charge_token = stripe.Token.create(
            card={
                "number": '4242424242424242',
                "exp_month": 12,
                "exp_year": 2019,
                "cvc": '123'
            },
        )
        print('created token')

    print('creating customer')
    customer = stripe.Customer.create(
        email='john@thinklumo.com',
        source=charge_token,
    )
    print('created customer')

    subscription = stripe.Subscription.create(
        customer=customer.id,
        items=[{'plan': payment_plan_id}],
    )
    print('created subscription')

    return subscription
