Stripe prototype for "Adelman upgrade" project
==============================================

This app explores Stripes scheduled billing features.

# Installation
```
virtualenv --python=python3 venv-py3
. venv-py3/bin/activate
pip install -r requirements.txt
```

# Running the app
## Running locally
```
chalice local --port=8081
```

## Running on AWS
```
chalice deploy

Regen deployment package.
Updating IAM policy for role: stripe-prototype-dev
Updating lambda function: stripe-prototype-dev
API Gateway rest API already found: zxb8vnce4h
Deploying to API Gateway stage: api
https://zxb8vnce4h.execute-api.us-west-2.amazonaws.com/api/
```

The api has been deployed but you'll need a charge token. Note: charge tokens
can only be used once. Charge tokens are normally obtained from Stripe.js but
I've included a script that'll generate a charge token from a test credit card
because the frontend isn't ready yet.

```
./generate-stripe-charge-token.py

tok_1CDhmXGrr0J6V9ZtKCLhLLax
```

Now that you have a token, send it to the api and the api will charge that token $10 and schedule a monthly rebill.

```
(venv-py3) Johns-MBP:stripe-prototype jkt$ curl https://zxb8vnce4h.execute-api.us-west-2.amazonaws.com/api/?charge_token=tok_1CDhmXGrr0J6V9ZtKCLhLLax | jq
{
  "id": "sub_Ccxikj4UU5W6vl",
  "object": "subscription",
  "application_fee_percent": null,
  "billing": "charge_automatically",
  "billing_cycle_anchor": 1522972006,
  "cancel_at_period_end": false,
  "canceled_at": null,
  "created": 1522972006,
  "current_period_end": 1525564006,
  "current_period_start": 1522972006,
  "customer": "cus_CcxiEDYJvnTnaf",
  "days_until_due": null,
  "discount": null,
  "ended_at": null,
  "items": {
    "object": "list",
    "data": [
      {
        "id": "si_CcxieangEp0344",
        "object": "subscription_item",
        "created": 1522972007,
        "metadata": {},
        "plan": {
          "id": "plan_CcwDsseYD6gY7n",
          "object": "plan",
          "amount": 1000,
          "billing_scheme": "per_unit",
          "created": 1522966453,
          "currency": "usd",
          "interval": "month",
          "interval_count": 1,
          "livemode": false,
          "metadata": {},
          "nickname": "Illumonati monthly membership fee",
          "product": "prod_CcwDg0KmziLThu",
          "tiers": null,
          "tiers_mode": null,
          "transform_usage": null,
          "trial_period_days": null,
          "usage_type": "licensed"
        },
        "quantity": 1,
        "subscription": "sub_Ccxikj4UU5W6vl"
      }
    ],
    "has_more": false,
    "total_count": 1,
    "url": "/v1/subscription_items?subscription=sub_Ccxikj4UU5W6vl"
  },
  "livemode": false,
  "metadata": {},
  "plan": {
    "id": "plan_CcwDsseYD6gY7n",
    "object": "plan",
    "amount": 1000,
    "billing_scheme": "per_unit",
    "created": 1522966453,
    "currency": "usd",
    "interval": "month",
    "interval_count": 1,
    "livemode": false,
    "metadata": {},
    "nickname": "Illumonati monthly membership fee",
    "product": "prod_CcwDg0KmziLThu",
    "tiers": null,
    "tiers_mode": null,
    "transform_usage": null,
    "trial_period_days": null,
    "usage_type": "licensed"
  },
  "quantity": 1,
  "start": 1522972006,
  "status": "active",
  "tax_percent": null,
  "trial_end": null,
  "trial_start": null
}
```


