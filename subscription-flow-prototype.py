#!/usr/bin/env python3
import stripe

stripe.api_key = 'sk_test_8MWyRiyTsBirGJcvaHUYSWf0'

product = stripe.Product.create(
    name='Illumonati membership',
    type='service',
    statement_descriptor='LUMO - get some'
)

plan = stripe.Plan.create(
    product=product.id,
    nickname='Illumonati monthly membership fee',
    interval='month',
    currency='usd',
    amount=1000,  # the amount in cents
)

# This would normally be obtained from Stripe.js
token = stripe.Token.create(
    card={
        "number": '4242424242424242',
        "exp_month": 12,
        "exp_year": 2019,
        "cvc": '123'
    },
)

# Normal flow starts here
customer = stripe.Customer.create(
    email='john@thinklumo.com',
    source=token.id,
)

subscription = stripe.Subscription.create(
    customer=customer.id,
    items=[{'plan': plan.id}],
)
